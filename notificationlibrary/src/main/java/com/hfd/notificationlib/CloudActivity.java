package com.hfd.notificationlib;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class CloudActivity extends Activity {
	
	Point size;
	Typeface font;
	
	int secondToWait = 5;
	boolean enabled = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cloud);

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.build();
		ImageLoader.getInstance().init(config);
		
		
		
		Display display = getWindowManager().getDefaultDisplay();
		size = new Point();

		size = getDisplaySize(display);
		
		
		
		
		
		
		
		
		final String link = getIntent().getExtras().getString("link");
				
		
		
		final ImageView img  = (ImageView) findViewById(R.id.imageViewCloud);
		LayoutParams params = new LayoutParams(
				(int) (size.x/1.2f), (int) (size.x/1.2f));
		img.setLayoutParams(params);
		
		img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent happyfaceIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + link));
//				startActivity(happyfaceIntent);
//				CloudActivity.this.finish();

				try {
					startActivity(happyfaceIntent);
					CloudActivity.this.finish();
				} catch (ActivityNotFoundException ex) {

				}

			}
		});
		
		
		
		
		
		String image = getIntent().getExtras().getString("image");
		image = image.replaceAll("\n", "");
		ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
		imageLoader.displayImage(image, img);


//		ImageManager imageManager = ImageManager
//				.getInstance(CloudActivity.this);
//		imageManager.get(image, new OnImageReceivedListener() {
//
//			@Override
//			public void onImageReceived(String source, Bitmap bitmap) {
//				img.setImageBitmap(bitmap);
//			}
//		});
		
		
		
		
		
		final Button btnClose = (Button) findViewById(R.id.buttonCloud);
		btnClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CloudActivity.this.finish();				
			}
		});
		
		
		
		
		
		String title = getIntent().getExtras().getString("title");
		font = Typeface.createFromAsset(getAssets(), "Roboto-Thin.ttf");
		
		TextView tv = (TextView) findViewById(R.id.textViewCloud);
		tv.setTypeface(font);
		tv.setText(title);
		
		btnClose.setEnabled(false);
		enabled = false;
		
		
		 new CountDownTimer(secondToWait * 1000, 1000) {

		     public void onTick(long millisUntilFinished) {
		    	 btnClose.setText("Asteapta: " + millisUntilFinished / 1000);
		     }

		     public void onFinish() {
		    	 btnClose.setText("Inchide");
		    	 btnClose.setEnabled(true);
		    	 enabled = true;
		     }
		  }.start();
		
		
		
		
	}
	
	
	
	
	@Override
	public void onBackPressed() {
		if (enabled)
		super.onBackPressed();
	}
	
	
	
	
	
	@SuppressLint("NewApi")
	private static Point getDisplaySize(final Display display) {
		final Point point = new Point();
		try {
			display.getSize(point);
		} catch (NoSuchMethodError ignore) { // Older device
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		return point;
	}



}
