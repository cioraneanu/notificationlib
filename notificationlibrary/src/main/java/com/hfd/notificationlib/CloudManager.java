package com.hfd.notificationlib;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

public class CloudManager {
	
//	private final String URL = "http://www.happyfacedevs.com/updates/update.php";
	boolean showAds = false;
	String id, title, link, image;
	
	Activity activity;
	
	SharedPreferences mPrefs;
	SharedPreferences.Editor mEditor;

	
	public CloudManager(Activity activity){
		
		this.activity = activity;		
		mPrefs = activity.getSharedPreferences("prefs", activity.MODE_PRIVATE);
		mEditor = mPrefs.edit();
		
		
		showAds = false;
		
	}
	
	
	public void checkForUpdates(String URL){
		new CheckForMessage().execute(URL);
	}
	
	
	
	
	public void showNotificationIfPossible(){
		
		if (showAds) {
			showAds = false;
			mEditor.putBoolean(id, true);
			mEditor.commit();
			showImageDailog(image, title, link);
		}
		
		
//		showImageDailog("", "VASILEEE", "dsadas");
		
	}
	
	
	
	
	
	
	
	private class CheckForMessage extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {

			String line = "";
			StringBuilder text = new StringBuilder();

			try {

				URL url = new URL(params[0]);
				URLConnection conn = url.openConnection();
				// Get the response
				BufferedReader rd = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				while ((line = rd.readLine()) != null) {
					text.append(line + "\n");

				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			return text.toString();
		}

		@Override
		protected void onPostExecute(String result) {

			if (result != null)
				if (result != "") {

					id = result.substring(0, result.indexOf("\n"));
					result = result.substring(result.indexOf("\n") + 1);

					title = result.substring(0, result.indexOf("\n"));
					result = result.substring(result.indexOf("\n") + 1);

					link = result.substring(0, result.indexOf("\n"));
					result = result.substring(result.indexOf("\n") + 1);

					image = result;

					boolean alreadyShown = mPrefs.getBoolean(id, false);

					if (alreadyShown == false) {

						showAds = true;
					}
					showAds = true;
				}

		}

	}

	private void showImageDailog(String image, String title, String link) {

		Intent intent = new Intent(activity, CloudActivity.class);
		intent.putExtra("image", image);
		intent.putExtra("title", title);
		intent.putExtra("link", link);
		activity.startActivity(intent);

	}
	
	
	
	

}
